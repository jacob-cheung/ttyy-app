import React from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { useTailwind } from 'tailwind-rn'
import { useTranslation } from 'react-i18next'

//  Components
import Auth from '@components/Profile/Auth'

const Profile = () => {
    const dispatch = useDispatch()
    const tailwind = useTailwind()
    const { t } = useTranslation('profile')

    return (
        <></>
    )
}

export default Profile
