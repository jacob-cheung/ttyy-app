import React from 'react'
import { View, ScrollView } from 'react-native'

import { useTailwind } from 'tailwind-rn'

//  Components
import GiveawayLocation from '@components/Home/GiveawayLocation'
import GiveawayCarousel from '@components/Home/GiveawayCarousel'
import HotSales from '@components/Home/HotSales'
import News from '@components/Home/News'

const Home = () => {
    const tailwind = useTailwind()

    return (
        <ScrollView style={tailwind('flex')}>
            <GiveawayLocation />
            <View style={tailwind('bg-white h-full py-4')}>
                <GiveawayCarousel style={tailwind('mb-6')} />
                <HotSales style={tailwind('mb-6')} />
                <News />
            </View>
        </ScrollView>
    )
}

export default Home
