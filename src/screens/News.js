import React from 'react'
import { ScrollView } from 'native-base'

import { useTailwind } from 'tailwind-rn'

//  Components 
import NewsList from '@components/News/NewsList'

const News = () => {
    const tailwind = useTailwind()

    return (
        <ScrollView style={tailwind('p-4 bg-white')}>
            <NewsList />
        </ScrollView>
    )
}

export default News
