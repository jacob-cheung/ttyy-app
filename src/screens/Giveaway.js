import React from 'react'
import { Text } from 'react-native'

import { SafeAreaView } from 'react-native-safe-area-context'

const Redeem = () => {
    return (
        <SafeAreaView>
            <Text>Redeem Page</Text>
        </SafeAreaView>
    )
}

export default Redeem
