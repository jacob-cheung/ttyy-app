import * as type from '../types'

const initialState = {
    items: null,
    error: null,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case type.FETCH_NEWS_SUCCESS:
            return {
                ...state,
                items: action.payload,
                error: null,
            }
        case type.FETCH_NEWS_FAIL:
            return {
                ...state,
                items: null,
                error: action.error,
            }
        default:
            return { ...state }
    }
}
