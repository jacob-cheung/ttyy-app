import * as type from '../types'

const initialState = {
    items: null,
    error: null,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case type.FETCH_HOT_SALES_SUCCESS:
            return {
                ...state,
                items: action.payload,
                error: null,
            }
        case type.FETCH_HOT_SALES_FAIL:
            return {
                ...state,
                items: null,
                error: action.error,
            }
        default:
            return { ...state }
    }
}
