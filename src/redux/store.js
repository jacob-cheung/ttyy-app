import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'

import * as reducers from '@reducers/*.js'
import rootSaga from './sagas/rootSaga'


const sagaMiddleware = createSagaMiddleware()

const store = createStore(
    combineReducers(reducers),
    applyMiddleware(sagaMiddleware),
)

sagaMiddleware.run(rootSaga)

export default store
