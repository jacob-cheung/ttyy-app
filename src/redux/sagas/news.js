import * as type from '../types'
import { getNews } from '@utils/apiService'
import { fetchNewsSuccess, fetchNewsFail } from '@actions/news'
import { call, put, takeEvery } from 'redux-saga/effects'

function* getNewsWorker() {
    try {
        const response = yield call(getNews)
        yield put(fetchNewsSuccess(response.data))
    } catch (e) {
        yield put(fetchNewsFail(e))
    }
}

function* getNewsWatcher() {
    yield takeEvery(type.FETCH_NEWS, getNewsWorker)
}

export default getNewsWatcher
