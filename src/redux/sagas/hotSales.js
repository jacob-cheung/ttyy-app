import * as type from '../types'
import { getHotSales } from '@utils/apiService'
import { fetchHotSalesSuccess, fetchHotSalesFail } from '@actions/hotSales'
import { call, put, takeEvery } from 'redux-saga/effects'

function* getHotSalesWorker() {
    try {
        const response = yield call(getHotSales)
        yield put(fetchHotSalesSuccess(response.data))
    } catch (e) {
        yield put(fetchHotSalesFail(e))
    }
}

function* getHotSalesWatcher() {
    yield takeEvery(type.FETCH_HOT_SALES, getHotSalesWorker)
}

export default getHotSalesWatcher
