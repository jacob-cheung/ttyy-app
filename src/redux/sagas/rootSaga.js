import { all } from 'redux-saga/effects'
import giveawaySaga from '@sagas/giveaway'
import hotSalesSaga from '@sagas/hotSales'
import newsSaga from '@sagas/news'

function* rootSaga() {
    yield all([
        // Sagas
        giveawaySaga(),
        hotSalesSaga(),
        newsSaga(),
    ])
}

export default rootSaga
