import * as type from '../types'
import { getGiveaway } from '@utils/apiService'
import { fetchGiveawaySuccess, fetchGiveawayFail } from '@actions/giveaway'
import { call, put, takeEvery } from 'redux-saga/effects'

function* getGiveawayWorker() {
    try {
        const response = yield call(getGiveaway)
        yield put(fetchGiveawaySuccess(response.data))
    } catch (e) {
        yield put(fetchGiveawayFail(e))
    }
}

function* getGiveawayWatcher() {
    yield takeEvery(type.FETCH_GIVEAWAY, getGiveawayWorker)
}

export default getGiveawayWatcher

// function* giveawaySaga() {
//     yield all([
//         getGiveawayWatcher(),
//     ])
// }
//
// export default giveawaySaga
