import * as type from '../types'

export function fetchNews() {
    return { type: type.FETCH_NEWS }
}

export function fetchNewsSuccess(payload) {
    return { type: type.FETCH_NEWS_SUCCESS, payload }
}

export function fetchNewsFail(error) {
    return { type: type.FETCH_NEWS_FAIL, error }
}
