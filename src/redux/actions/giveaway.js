import * as type from '../types'

export function fetchGiveaway() {
    return { type: type.FETCH_GIVEAWAY }
}

export function fetchGiveawaySuccess(payload) {
    return { type: type.FETCH_GIVEAWAY_SUCCESS, payload }
}

export function fetchGiveawayFail(error) {
    return { type: type.FETCH_GIVEAWAY_FAIL, error }
}
