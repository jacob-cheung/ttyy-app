import * as type from '../types'

export function fetchHotSales() {
    return { type: type.FETCH_HOT_SALES }
}

export function fetchHotSalesSuccess(payload) {
    return { type: type.FETCH_HOT_SALES_SUCCESS, payload }
}

export function fetchHotSalesFail(error) {
    return { type: type.FETCH_HOT_SALES_FAIL, error }
}
