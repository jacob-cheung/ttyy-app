import React from 'react'
import { StyleSheet } from 'react-native'
import { View, Image, Text } from 'native-base'

import { useTailwind } from 'tailwind-rn'
import { useTranslation } from 'react-i18next'

const NewsItem = ({ item, style }) => {
    const tailwind = useTailwind()
    const { i18n } = useTranslation()

    if (!item) return (<></>)

    return (
        <View style={[tailwind('flex p-4 rounded-xxl'), newsStyles.news_item, style || {}]}>
            <Image
                style={[tailwind('h-full w-full rounded-xxl mb-4'), newsStyles.news_thumbnail]}
                source={{ uri: item.thumbnail }}
                alt={item.title[i18n.language]}
            />
            <Text style={tailwind('text-xs mb-2')}>{item.title[i18n.language]}</Text>
            <Text style={tailwind('text-xxs')}>{item.description[i18n.language]}</Text>
        </View>
    )
}

const newsStyles = StyleSheet.create({
    news_item: {
        maxHeight: 416,
        borderWidth: 1,
        borderColor: 'rgba(115, 151, 75, 0.6)',

    },
    news_thumbnail: {
        maxHeight: 234,
    },
})

export default NewsItem
