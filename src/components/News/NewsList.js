import React, { useEffect } from 'react'
import { View, Text } from 'native-base'
import { fetchNews } from '@actions/news'
import { map } from 'lodash'

import { useDispatch, useSelector } from 'react-redux'
import { useTailwind } from 'tailwind-rn'
import { useTranslation } from 'react-i18next'

//  Components
import NewsItem from '@components/News/NewsItem'

const NewsList = () => {
    const dispatch = useDispatch()
    const tailwind = useTailwind()
    const { t } = useTranslation('news')

    const { items, error } = useSelector(state => state.news)

    useEffect(() => {
        if (!items) {
            dispatch(fetchNews())
        }
    }, [])

    return (
        <View>
            <Text style={tailwind('text-xl mb-6')}>{t('title')}</Text>
            {map(items, (item, index) => <NewsItem item={item} style={tailwind('mb-8')} key={index} />)}
        </View>
    )
}

export default NewsList
