import React, { useState, useEffect } from 'react'
import { Dimensions, StyleSheet } from 'react-native'
import { View, ScrollView, Image, Text } from 'native-base'
import { fetchGiveaway } from '@actions/giveaway'
import { map } from 'lodash'

import { useDispatch, useSelector } from 'react-redux'
import { useTailwind } from 'tailwind-rn'
import { useTranslation } from 'react-i18next'

export const GiveawayItem = ({ item, style }) => {
    const tailwind = useTailwind()
    const { i18n } = useTranslation()

    if (!item) return (<></>)

    return (
        <View style={[tailwind('justify-center rounded-md mr-2'), giveawayStyles.giveaway_item, style || {}]}>
            <Image
                style={giveawayStyles.giveaway_item_thumbnail}
                source={{ uri: 'https://wallpaperaccess.com/full/317501.jpg' || item.thumbnail }}
                alt={item.title[i18n.language]}
                size='md'
            />
            <Text style={tailwind('text-xxs text-center leading-3 p-2')}>{item.title[i18n.language]}</Text>
        </View>
    )
}

const GiveawayCarousel = () => {
    const dispatch = useDispatch()
    const tailwind = useTailwind()
    const { t } = useTranslation('home')

    const { items, error } = useSelector(state => state.giveaway)
    const [dimensions, setDimensions] = useState(Dimensions.get('window'))

    useEffect(() => {
        if (!items) {
            dispatch(fetchGiveaway())
        }
    }, [])

    return (
        <>
            <Text style={tailwind('px-4 mb-4')} fontSize="xl">{t('currentGiveaway')}</Text>
            <ScrollView
                style={tailwind('grow-0 mb-10')}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                children={map(items, (item, index) => <GiveawayItem item={item} key={index} style={index === 0 ? tailwind('ml-4') : {}} />)}
            />
        </>
    )
}

const giveawayStyles = StyleSheet.create({
    giveaway_item: {
        maxWidth: 74,
        maxHeight: 114,
        borderWidth: 1,
        borderColor: 'rgba(115, 151, 75, 0.1)',
    },
    giveaway_item_thumbnail: {
        height: 78,
    },
})

export default GiveawayCarousel
