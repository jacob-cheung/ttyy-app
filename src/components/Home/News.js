import React, { useEffect } from 'react'
import { View, Text, Button } from 'native-base'
import { fetchNews } from '@actions/news'

import { useDispatch, useSelector } from 'react-redux'
import { useNavigation } from '@react-navigation/native'
import { useTailwind } from 'tailwind-rn'
import { useTranslation } from 'react-i18next'

//  Components
import NewsItem from '@components/News/NewsItem'

const News = () => {
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const tailwind = useTailwind()
    const { t } = useTranslation('home')

    const { items, error } = useSelector(state => state.news)

    useEffect(() => {
        if (!items) {
            dispatch(fetchNews())
        }
    }, [])

    return (
        <>
            <View style={tailwind('flex flex-row justify-between px-4')}>
                <Text fontSize="xl">{t('news')}</Text>
                <Button variant="ghost" colorScheme="light" onPress={() => navigation.navigate('News')}>
                    <Text style={tailwind('text-green-apple')}>{t('common:readAll')}</Text>
                </Button>
            </View>
            <View style={tailwind('flex px-4 justify-center')}>
                <NewsItem item={items && items[0]} />
            </View>
        </>
    )
}

export default News
