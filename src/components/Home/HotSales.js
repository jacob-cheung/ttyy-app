import React, { useEffect } from 'react'
import { StyleSheet, ImageBackground } from 'react-native'
import { View, ScrollView, Image, Text, Button } from 'native-base'
import { fetchHotSales } from '@actions/hotSales'
import { map } from 'lodash'

import { useDispatch, useSelector } from 'react-redux'
import { useTailwind } from 'tailwind-rn'
import { useTranslation } from 'react-i18next'

export const HotItem = ({ item, style }) => {
    const tailwind = useTailwind()
    const { i18n } = useTranslation()

    if (!item) return (<></>)

    return (
        <>
            <View style={[tailwind('relative mr-6'), hotSalesStyles.hot_item, style || {}]}>
                <ImageBackground
                    style={tailwind('flex-1 px-4')}
                    imageStyle={tailwind('rounded-lg')}
                    source={{ uri: item.thumbnail }}
                >
                    <Text style={tailwind('absolute bottom-2 left-4 text-white text-lg')}>{item.title[i18n.language]}</Text>
                </ImageBackground>
            </View>
        </>
    )
}

const HotSales = () => {
    const dispatch = useDispatch()
    const tailwind = useTailwind()
    const { t } = useTranslation('home')

    const { items, error } = useSelector(state => state.hotSales)

    useEffect(() => {
        if (!items) {
            dispatch(fetchHotSales())
        }
    }, [])

    return (
        <>
            <View style={tailwind('flex flex-row justify-between px-4')}>
                <Text fontSize="xl">{t('hotSales')}</Text>
                <Button variant="ghost" colorScheme="light">
                    <Text style={tailwind('text-green-apple')}>{t('common:readAll')}</Text>
                </Button>
            </View>
            <ScrollView
                style={tailwind('grow-0 mb-10')}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                children={map(items, (item, index) => <HotItem item={item} key={index} style={index === 0 ? tailwind('ml-4') : {}} />)}
            />
        </>
    )
}

const hotSalesStyles = StyleSheet.create({
    hot_item: {
        maxHeight: 160,
        maxWidth: 200,
        height: 160,
        width: 200,
    },
})

export default HotSales
