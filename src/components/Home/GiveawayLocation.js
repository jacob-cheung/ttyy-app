import React from 'react'
import { StyleSheet } from 'react-native'
import { View, Text, Button, Icon } from 'native-base'

import { useNavigation } from '@react-navigation/native'
import { useTailwind } from 'tailwind-rn'
import { useTranslation } from 'react-i18next'

import SearchLocationIcon from '@components/Icon/SearchLocation'

const GiveawayLocation = () => {
    const navigation = useNavigation()
    const tailwind = useTailwind()
    const { t } = useTranslation('home')

    return (
        <View style={tailwind('p-4')}>
            <Text bold fontSize="4xl">{t('receiveGiveaway')}</Text>
            <Button
                leftIcon={<SearchLocationIcon style={styles.icon} />}
                style={tailwind('rounded-lg bg-green-apple')}
                onPress={() => navigation.jumpTo('Giveaway')}
            >
                <Text style={tailwind('text-white')} fontSize="lg">{t('searchGiveawayLocation')}</Text>
            </Button>
        </View>
    )
}

const styles = StyleSheet.create({
    icon: {
        maxWidth: 22,
        maxHeight: 22,
    },
})

export default GiveawayLocation
