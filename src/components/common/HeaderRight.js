import React from 'react'
import { StyleSheet } from 'react-native'
import { Center, Button, Text } from 'native-base'

import { useTailwind } from 'tailwind-rn'
import { useTranslation } from 'react-i18next'

import QRCodeIcon from '@components/Icon/QRCode'

const HeaderRight = () => {
    const tailwind = useTailwind()
    const { t } = useTranslation()

    return (
        <Button variant="ghost" colorScheme="light" style={tailwind('p-0 mx-3')}>
            <Center>
                <QRCodeIcon style={styles.icon} />
            </Center>
            <Text fontSize="xs" style={tailwind('text-green-apple')}>{t('scanQRCode')}</Text>
        </Button>
    )
}

const styles = StyleSheet.create({
    icon: {
        maxHeight: 25,
        maxWidith: 25,
    },
})

export default HeaderRight
