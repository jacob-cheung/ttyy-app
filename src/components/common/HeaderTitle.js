import React from 'react'
import { StyleSheet, Image } from 'react-native'

import { useTailwind } from 'tailwind-rn'

//  Images
const logoImage = require('../../assets/images/ttyy-logo.png')

const HeaderTitle = () => {
    const tailwind = useTailwind()

    return (
        <Image
            style={[styles.logo]}
            source={logoImage}
        />
    )
}

const styles = StyleSheet.create({
    logo: {
        maxHeight: 45,
        aspectRatio: 31 / 15,
    },
})

export default HeaderTitle
