import axios from 'axios'

const http = axios.create({
    baseURL: 'http://localhost:4001/api',
})

export const getGiveaway = () => {
    return http.get('/giveaway').then(res => res)
}

export const getHotSales = () => {
    return http.get('hot_sales')
}

export const getNews = () => {
    return http.get('news')
}
