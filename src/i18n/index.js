import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { getLocales } from 'react-native-localize'
import { forEach } from 'lodash'

import * as files from './locale/*.js'
const resources = {}

var deviceLanguage = 'en'
const currentLocale = getLocales()[0]

if (currentLocale.languageCode === 'zh') {
    if (currentLocale.scriptCode === 'Hant') {
        deviceLanguage = 'zhhk'
    } else {
        deviceLanguage = 'zhcn'
    }
}

forEach(files, (value, key) => {
    if (key === 'index') return
    resources[key] = value
})

i18n
    .use(initReactI18next)
    .init({
        lng: deviceLanguage,
        fallbackLng: deviceLanguage,
        resources,
        interpolation: {
            escapeValue: false,
        },
        ns: ['common'],
        defaultNS: 'common',
    })
