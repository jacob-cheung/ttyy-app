export default {
    common: {
        home: '首頁',
        giveaway: '換領',
        shopping: '購物',
        news: '消息',
        profile: '我的',
        scanQRCode: '掃描售賣機',
        readAll: '查看全部',
    },
    home: {
        receiveGiveaway: '領取贈品',
        searchGiveawayLocation: '搜尋附近派發點',
        currentGiveaway: '選擇今期贈品',
        hotSales: '熱賣產品',
        news: '最新消息',
    },
    giveaway: {},
    shopping: {},
    news: {
        title: '最新消息',
    },
    profile: {},
}
