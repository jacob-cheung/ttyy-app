import React from 'react'

//  Redux
import { Provider } from 'react-redux'
import store from './redux/store'

// Nativebase
import { NativeBaseProvider } from 'native-base'

// Navigation
import { NavigationContainer } from '@react-navigation/native'
import AppNavigator from './navigation'

// Tailwind
import { TailwindProvider } from 'tailwind-rn'
import utilities from '../tailwind.json'

const App = () => {
    return (
        <NativeBaseProvider>
            <TailwindProvider utilities={utilities}>
                <Provider store={store}>
                    <NavigationContainer>
                        <AppNavigator />
                    </NavigationContainer>
                </Provider>
            </TailwindProvider>
        </NativeBaseProvider>
    )
}

export default App
