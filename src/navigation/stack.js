import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import * as Screens from '@screens/*.js'

const HomeStack = createNativeStackNavigator()

export const HomeStackScreen = () => (
    <HomeStack.Navigator
        initialRouteName="Home"
        screenOptions={{
            headerShown: false,
        }}
    >
        <HomeStack.Screen name="Home" component={Screens.Home} />
    </HomeStack.Navigator>
)

const NewsStack = createNativeStackNavigator()

export const NewsStackScreen = () => (
    <NewsStack.Navigator
        initialRouteName="News"
        screenOptions={{
            headerShown: false,
        }}
    >
        <NewsStack.Screen name="News" component={Screens.News} />
    </NewsStack.Navigator>
)

const ProfileStack = createNativeStackNavigator()

export const ProfileStackScreen = () => (
    <ProfileStack.Navigator
        initialRouteName="Profile"
        screenOptions={{
            headerShown: false,
        }}
    >
        <ProfileStack.Screen name="Profile" component={Screens.Profile} />
    </ProfileStack.Navigator>
)
