import React from 'react'
import { StyleSheet } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import * as Screens from '@screens/*.js'
import * as StackScreens from './stack'

import { useTranslation } from 'react-i18next'

const Tab = createBottomTabNavigator()

//  Components
import HeaderTitle from '@components/common/HeaderTitle'
import HeaderRight from '@components/common/HeaderRight'

//  Icon
import HomeIcon from '@components/Icon/Home'
import GiveawayIcon from '@components/Icon/Giveaway'
import ShoppingIcon from '@components/Icon/Shopping'
import NewsIcon from '@components/Icon/News'
import ProfileIcon from '@components/Icon/Profile'

const AppNavigator = () => {
    const { t } = useTranslation()

    return (
        <Tab.Navigator
            initialRouteName="Home"
            screenOptions={{
                headerStyle: {
                    backgroundColor: 'transparent',
                },
                tabBarActiveTintColor: '#73B54B',
                headerTitle: () => <HeaderTitle />,
                headerRight: () => <HeaderRight />,
            }}
        >
            <Tab.Screen
                name="HomeTab"
                component={StackScreens.HomeStackScreen}
                options={{
                    title: t('home'),
                    tabBarIcon: ({ color }) => (
                        <HomeIcon color={color} style={styles.icon} />
                    ),
                }}
            />
            <Tab.Screen
                name="Giveaway"
                title={t('giveaway')}
                component={Screens.Giveaway}
                options={{
                    headerShown: false,
                    title: t('giveaway'),
                    tabBarIcon: ({ color }) => (
                        <GiveawayIcon color={color} style={styles.icon} />
                    ),
                }}
            />
            <Tab.Screen
                name="Shopping"
                component={Screens.Shopping}
                options={{
                    title: t('shopping'),
                    tabBarIcon: ({ color }) => (
                        <ShoppingIcon color={color} style={styles.icon} />
                    ),
                }}
            />
            <Tab.Screen
                name="NewsTab"
                component={StackScreens.NewsStackScreen}
                options={{
                    title: t('news'),
                    tabBarIcon: ({ color }) => (
                        <NewsIcon color={color} style={styles.icon} />
                    ),
                }}
            />
            <Tab.Screen
                name="ProfileTab"
                component={StackScreens.ProfileStackScreen}
                options={{
                    title: t('profile'),
                    tabBarIcon: ({ color }) => (
                        <ProfileIcon color={color} style={styles.icon} />
                    ),
                }}
            />
        </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    icon: {
        maxHeight: 25,
        maxWidth: 25,
    },
})

export default AppNavigator
