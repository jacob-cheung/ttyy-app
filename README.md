# TTYY-App

TTYY react native app

require Android version >= ?<br />
require IOS version >= ?<br/>

## Installation
```sh
# install node_module
$ yarn

# install pod packages
npx pod-install

# build tailwind.json
$ yarn build:tailwind

# start the app
npx react-native run-ios        # on IOS
npx react-native run-android    # on Android
```

## Develop

Preparing for development
```sh
# concurrently rebuild tailwind.json
$ yarn dev:tailwind
```
