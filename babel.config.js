module.exports = {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
        [
            'import-glob',
        ],
        [
            'module-resolver',
            {
                alias: {
                    '@components': './src/components',
                    '@screens': './src/screens',
                    '@actions': './src/redux/actions',
                    '@reducers': './src/redux/reducers',
                    '@sagas': './src/redux/sagas',
                    '@assets': './src/assets',
                    '@utils': './src/utils',
                },
            },
        ],
    ],
}
