/**
 * @format
 */
import React from 'react'
import { AppRegistry } from 'react-native'

//  Components
import App from './src/App'

//  I18next
import i18n from './src/i18n'

import { name as appName } from './app.json'

AppRegistry.registerComponent(appName, () => App)
