module.exports = {
    content: [
        './src/*.js',
        './src/**/*.js',
    ],
    theme: {
        extend: {
            colors: {
                'green-apple': 'rgb(115, 151, 75)',
            },
            fontSize: {
                'xxs': '.625rem',
                'xxxs': '.5rem',
            },
            borderRadius: {
                'xl': '0.625rem',
                'xxl': '0.75rem',
            },
        },
    },
    plugins: [],
    corePlugins: {
        transform: false,
        translate: false,
        boxShadow: false,
    },
}
